import 'dart:typed_data';
import 'package:dart_base_x/dart_base_x.dart';

const alphabet = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz';

// inspired from bitcoin style leading zero compression
String getBaseXEncodedString(String str) {
  final List<int> codeUnits = str.codeUnits;
  final Uint8List unit8List = Uint8List.fromList(codeUnits);
  BaseXCodec bs58 = BaseXCodec(alphabet);
  return bs58.encode(unit8List);
}

String getBaseXDecodedString(String str) {
  BaseXCodec bs58 = BaseXCodec(alphabet);
  final Uint8List unit8List = bs58.decode(str);
  return String.fromCharCodes(unit8List);
}