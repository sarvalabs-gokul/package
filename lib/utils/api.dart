import 'req_wrapper.dart';
import 'common.dart';

const invalidAddress = "0x0000000000000000000000000000000000000000";

Future<String> getUserID(String uN) async {
  String encodedUsername = getBaseXEncodedString(uN);
  var userNameResponse = await sendRequest(
      true, 'identity', 'getdefaultaddress?username=$encodedUsername', Null);
  if (userNameResponse.statusCode == 200) {
    if (userNameResponse.body == invalidAddress) {
      throw "invalid username";
    }
    return userNameResponse.body;
  }
  throw userNameResponse.body;
}

Future<Object> getZkProofOfUser(String userID) async {
  var getZkProof = await sendRequest(false, 'auth', 'getmks',
      <String, String>{'defAddr': userID, 'typeOfProof': 'zk'});
  if (getZkProof.statusCode == 200) {
    return Future<Object>.value(getZkProof.body);
  }
  throw getZkProof.body;
}

Future<Object> getWalletKeystore(String userID, dynamic ksAuthToken) async {
  var ksResponse = await sendRequest(false, 'auth', 'getmks', <String, dynamic>{
    'defAddr': userID,
    'typeOfProof': 'keystore',
    "authToken": ksAuthToken
  });
  if (ksResponse.statusCode == 200) {
    return Future<Object>.value(ksResponse.body);
  }
  throw ksResponse.body;
}

Future<Object> storeMks(dynamic zkProof, dynamic walletKeystore,
    dynamic cipherParams, String defAddr) async {
  Map<String, dynamic> mks = {
    'version': 1,
    '_proof': zkProof,
    '_keystore': walletKeystore,
    '_moiCipherParams': cipherParams,
  };

  var ksResponse =
      await sendRequest(false, 'auth', 'storemks', <String, dynamic>{
    'moiID': defAddr,
    'authJson': mks,
  });
  if (ksResponse.statusCode == 200) {
    return Future<Object>.value(ksResponse.body);
  }
  throw ksResponse.body;
}

Future<Object> registerUser(String userName, String defAddr) async {
  var ksResponse =
      await sendRequest(false, 'identity', 'register', <String, dynamic>{
    '_address': defAddr,
    '_username': userName,
  });
  if (ksResponse.statusCode == 200) {
    return Future<Object>.value(ksResponse.body);
  }
  throw ksResponse.body;
}
