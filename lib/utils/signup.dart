import 'dart:convert';

import 'req_wrapper.dart';
import 'common.dart';

Future<String> getUsersList() async {
  var userList = await sendRequest(true, 'list', 'users', Null);
  return userList.body.toString();
}

Future<bool> isUserNameAvailable(String userName) async {
  userName = userName.toLowerCase();
  String moiIdUsers = await getUsersList();
  List<dynamic> usersList = json.decode(moiIdUsers);
  List<String> userNames = usersList.asMap().entries.map((e) => getBaseXDecodedString(e.value)).toList();
  String user = userNames.firstWhere((element) => element == userName, orElse: () => "");
  if (user != "") {
    return Future<bool>.value(true);
  }
  return Future<bool>.value(false);
}
