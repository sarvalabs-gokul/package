import 'dart:typed_data';
import 'package:convert/convert.dart';
import 'package:bip32/bip32.dart' as bip32;
import 'package:bip39/bip39.dart' as bip39;
import 'package:pointycastle/digests/keccak.dart';
import 'package:pointycastle/ecc/curves/secp256k1.dart';
import 'package:pointycastle/ecc/api.dart';
import 'to_checksum.dart';
import 'privKey_To_BigInt.dart';

const moiIDPath = "m/44'/6174'/0'/0/0";
const defaultNameSpace = "_default_";

class InterGalacticPath {
  final String nameSpace;
  final String igcPath;

  const InterGalacticPath._(this.igcPath, this.nameSpace);
}

class Wallet {
  final String _defaultAddress;
  final String _mnemonic;
  final String _moiID;
  final InterGalacticPath _activeIgcPath;
  final bip32.BIP32 activeNode;

  const Wallet._(this._mnemonic, this._activeIgcPath, this._moiID,
      this._defaultAddress, this.activeNode);

  factory Wallet.createNew(String moiID) {
    String randomMnemonic = bip39.generateMnemonic();
    return Wallet.createFromSRP(randomMnemonic, moiID);
  }

  factory Wallet.createFromSRP(String twelveWordMnemonic, String moiID) {
    Uint8List seed = bip39.mnemonicToSeed(twelveWordMnemonic);
    bip32.BIP32 node = bip32.BIP32.fromSeed(seed);
    bip32.BIP32 child = node.derivePath(moiIDPath);

    Uint8List pubAddr = getPubAddressFromPrivKey(child.privateKey!);
    String pubAddrWithChecksum = toChecksum(bytesToHex(pubAddr, false));

    return Wallet._(
        twelveWordMnemonic,
        const InterGalacticPath._(moiIDPath, defaultNameSpace),
        moiID,
        pubAddrWithChecksum,
        child);
  }

  factory Wallet.createAndDerive(
      String twelveWordMnemonic, String moiID, String igcPath) {
    Uint8List seed = bip39.mnemonicToSeed(twelveWordMnemonic);
    bip32.BIP32 node = bip32.BIP32.fromSeed(seed);
    bip32.BIP32 child = node.derivePath(igcPath);

    Uint8List pubAddr = getPubAddressFromPrivKey(child.privateKey!);
    String pubAddrWithChecksum = toChecksum(bytesToHex(pubAddr, false));

    return Wallet._(twelveWordMnemonic, InterGalacticPath._(igcPath, ""), moiID,
        pubAddrWithChecksum, child);
  }

  String moiID() {
    return _moiID;
  }

  String defaultAddress() {
    return _defaultAddress;
  }

  InterGalacticPath activeIgcPath() {
    return _activeIgcPath;
  }

  Uint8List privateKey() {
    return activeNode.privateKey!;
  }

  Uint8List publicKey(bool compressed) {
    if (compressed) {
      bip32.BIP32 childNeutered = activeNode.neutered();
      return childNeutered.publicKey;
    } else {
      BigInt privKeyInUnsignedInt =
          decodeBigIntWithSign(1, activeNode.privateKey!);

      final ECDomainParameters params = ECCurve_secp256k1();
      final publicKey = (params.G * privKeyInUnsignedInt)!;
      Uint8List publicKeyBytesUncompressed =
          Uint8List.view(publicKey.getEncoded(false).buffer, 1);
      return publicKeyBytesUncompressed;
    }
  }

  String getEthereumChecksumAddress() {
    Uint8List pubAddr = getPubAddressFromPrivKey(activeNode.privateKey!);
    return toChecksum(bytesToHex(pubAddr, true));
  }
}

Uint8List getPubAddressFromPrivKey(Uint8List privBytes) {
  BigInt privKeyInUnsignedInt = decodeBigIntWithSign(1, privBytes);

  final ECDomainParameters params = ECCurve_secp256k1();
  final publicKey = (params.G * privKeyInUnsignedInt)!;
  Uint8List publicKeyBytes =
      Uint8List.view(publicKey.getEncoded(false).buffer, 1);

  final KeccakDigest keccakDigest = KeccakDigest(256);
  Uint8List hashed = keccakDigest.process(publicKeyBytes);
  return hashed.sublist(12, 32);
}

String bytesToHex(Uint8List tarBytes, bool shouldInclude0x) {
  var encoded = hex.encode(tarBytes);
  return (shouldInclude0x ? '0x' : '') + encoded;
}
