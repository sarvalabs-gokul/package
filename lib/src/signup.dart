import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:bip39/bip39.dart' as bip39;
import 'package:flutter/material.dart';
import 'package:convert/convert.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../utils/wallet.dart';
import '../utils/api.dart';
import '../utils/signup.dart';

class SignUp extends StatefulWidget {
  final Color primaryColor, secondaryColor, textColor;
  const SignUp(
      {Key? key,
      this.primaryColor = const Color(0xFF131B22),
      this.secondaryColor = const Color(0xFF6727BE),
      this.textColor = const Color(0xFFFFFFFF)})
      : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  final GlobalKey<FormState> _key = GlobalKey();
  int _currentStep = 0;

  String username = "";
  String password = "";
  String reTypedPassword = "";
  dynamic zkProof, walletKeystore, moiCipherParams;

  bool _passObscured = true;
  bool isSettingUp = false;
  bool _reTypedPassObscured = true;
  bool _isChecked = true;
  bool _isCopiedToClipboard = false;
  bool _isSecretPhraseCopied = false;
  bool _isValidationTextVisible = false;
  bool _isUserNameExists = false;
  bool _isUserNameValidating = false;
  int _selectedOption = -1;
  List<String> secretPhrase = List.filled(12, "secret");
  List<String> challengeOptions = [];
  List correctOrder = List<int>.filled(2, 0);
  Map<String, bool> passwordValidity = {
    "minimum_length": false,
    "number": false,
    "uppercase": false,
    "lowercase": false,
    "special_character": false
  };
  final FocusNode _focus = FocusNode();

  @override
  void initState() {
    super.initState();
    _focus.addListener(showValidationText);
  }

  @override
  void dispose() {
    super.dispose();
    _focus.removeListener(showValidationText);
    _focus.dispose();
  }

  void showValidationText() {
    setState(() {
      _isValidationTextVisible = _focus.hasFocus;
    });
  }

  void _togglePassObscured() {
    setState(() {
      _passObscured = !_passObscured; // Prevents focus if tap on eye
    });
  }

  void _toggleReTypedPassObscured() {
    setState(() {
      _reTypedPassObscured =
          !_reTypedPassObscured; // Prevents focus if tap on eye
    });
  }

  void _showToast(String msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0
    );
  }

  void _onStepContinue() async {
    void setupChallenge() {
      // Implementation of JS Lodash sampleSize
      List<String> sampleSize(List strArr, size) {
        final _random = Random();
        List<String> targetedSlice = List.filled(size, "");
        List _selectedSlice = List.filled(size, strArr.length);
        for (int i = 0; i < size; i++) {
          int randInt = _random.nextInt(strArr.length);
          int filteredEle = _selectedSlice
              .firstWhere((element) => element == randInt, orElse: () => -1);
          if (filteredEle == -1) {
            _selectedSlice[i] = randInt;
          } else {
            i = i - 1;
          }
        }

        for (int j = 0; j < size; j++) {
          targetedSlice[j] = strArr[_selectedSlice[j]];
        }
        return targetedSlice;
      }

      // Setting the options
      List<String> _challengeOptions = List<String>.filled(4, "");
      for (int k = 0; k < 4; k++) {
        _challengeOptions[k] = (sampleSize(secretPhrase, 2)).join(",");
      }

      int correctAnswer = Random().nextInt(4);
      List correctOrderStrings = _challengeOptions[correctAnswer].split(",");

      List _correctOrder = List.filled(2, 0);
      int filteredEle = secretPhrase
          .indexWhere((element) => element == correctOrderStrings[0]);
      _correctOrder[0] = filteredEle + 1;
      filteredEle = secretPhrase
          .indexWhere((element) => element == correctOrderStrings[1]);
      _correctOrder[1] = filteredEle + 1;

      setState(() {
        challengeOptions = _challengeOptions;
        correctOrder = _correctOrder;
        _selectedOption = -1;
      });
    }

    if (_currentStep == 1) {
      setupChallenge();
    } else if (_currentStep == 2) {
      List<String> correctOptions = correctOrder.asMap().entries.map((entry) {
        return secretPhrase.elementAt(entry.value - 1);
      }).toList();
      if (challengeOptions[_selectedOption] == correctOptions.join(",")) {
        print(correctOptions.join(","));
        print("Challenge success");
        // TODO: Handle on success
        return;
      } else {
        setupChallenge();
        return;
      }
    }

    setState(() {
      _currentStep = _currentStep + 1;
    });
  }

  String _getTitle() {
    switch (_currentStep) {
      case 1:
        return "SECRET RECOVERY PHRASE";
      case 2:
        return "SOLVE THE CHALLENGE";
      default:
        return "REGISTRATION";
    }
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'IomeChannel',
        onMessageReceived: (JavascriptMessage message) {
          print(message.message);
          final Map<String, dynamic> jsonMessage = jsonDecode(message.message);
          if (jsonMessage['pub'] != null) {
            setState(() {
              zkProof = jsonMessage;
            });
          } else if (jsonMessage['address'] != null) {
            setState(() {
              walletKeystore = jsonMessage;
            });
          } else if (jsonMessage['iv'] != null) {
            setState(() {
              moiCipherParams = jsonMessage;
            });
          } else {
            setState(() {
              isSettingUp = false;
            });
            // todo: send error to parent component through onFailure(message.message)
          }
        });
  }

  Widget setupAccount() {
    return FutureBuilder<WebViewController>(
        future: _controller.future,
        builder: (BuildContext context,
            AsyncSnapshot<WebViewController> controller) {
          return ElevatedButton(
            onPressed: () async {
              try {
                if (_key.currentState!.validate()) {
                  if (_isUserNameExists) {
                    _showToast("Username already exists!");
                    return;
                  } else if (passwordValidity["minimum_length"] == false) {
                    _showToast("Password must be at least 8 characters!");
                    return;
                  } else if (passwordValidity["number"] == false) {
                    _showToast("Password must contain a number!");
                    return;
                  } else if (passwordValidity["uppercase"] == false) {
                    _showToast("Password must contain a uppercase letter!");
                    return;
                  } else if (passwordValidity["lowercase"] == false) {
                    _showToast("Password must contain a lowercase letter!");
                    return;
                  } else if (passwordValidity["special_character"] == false) {
                    _showToast("Password must contain a special character!");
                    return;
                  } else if (password != reTypedPassword) {
                    _showToast("Password and Confirm Password must match!");
                    return;
                  } else {
                    setState(() {
                      isSettingUp = true;
                    });

                    var _con = controller.data!;

                    // 1. Create random SRP
                    String randomMnemonic = bip39.generateMnemonic();
                    setState(() {
                      secretPhrase = randomMnemonic.split(" ");
                    });

                    Wallet tempWallet = Wallet.createAndDerive(
                        randomMnemonic,
                        username,
                        "m/44'/60'/0'/0/0"); // in MOI Id v0.1-alpha.0, we use private key at etherum path to encrypt keystore

                    String encKey = hex.encode(tempWallet.privateKey());

                    // 2. Generate wallet keystore
                    await _con.runJavascript('''
                        var walletKeystore = window.hckzp.mCrypto.getWalletKeystore("$encKey", "$randomMnemonic", "$password")
                        .then(ks => IomeChannel.postMessage(ks))
                        .catch(e => IomeChannel.postMessage({ error: e.message }));
                    ''');

                    // 3. Generate ZK proof
                    await _con.runJavascript('''
                        var zkpProofOfPassword = window.hckzp.generateZkp("$password");
                        IomeChannel.postMessage(zkpProofOfPassword);
                    ''');

                    Wallet moiWallet = Wallet.createFromSRP(
                      randomMnemonic,
                      username,
                    ); // in MOI Id v0.1-alpha.0, we use private key at etherum path to encrypt keystore

                    String moiWalletPrivKey =
                        "0x" + hex.encode(moiWallet.privateKey());
                    // 4. Generate MOI Cipher Params
                    await _con.runJavascript('''
                        var cipherParams = window.hckzp.mCrypto.getCipherParams("$moiWalletPrivKey");
                        IomeChannel.postMessage(JSON.stringify(cipherParams));
                    ''');

                    String defAddress = moiWallet.getEthereumChecksumAddress();
                    // 5. Generate MKS
                    await storeMks(
                        zkProof, walletKeystore, moiCipherParams, defAddress);

                    await registerUser(username, defAddress);

                    setState(() {
                      isSettingUp = false;
                    });

                    _onStepContinue();
                  }
                }
              } catch (e) {
                setState(() {
                  isSettingUp = false;
                });
              }
            },
            child: isSettingUp
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Setting up...',
                        style: TextStyle(fontSize: 16, color: widget.textColor),
                      ),
                      const SizedBox(width: 10),
                      SizedBox(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(
                            color: widget.textColor, strokeWidth: 3.0),
                      ),
                    ],
                  )
                : Text('NEXT',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: widget.textColor,
                    )),
            style: ElevatedButton.styleFrom(
                primary: widget.secondaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0))),
          );
        });
  }

  List<Widget> _getStep() {
    Container getNextButton() {
      return Container(
          margin: const EdgeInsets.only(top: 24.0),
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: ElevatedButton(
              child: const Text('NEXT',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              style: ElevatedButton.styleFrom(
                  primary: widget.secondaryColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0))),
              onPressed: _onStepContinue),
          height: 50.0,
          width: double.infinity);
    }

    List<Widget> getStep1() {
      return [
        Container(
            margin: const EdgeInsets.only(top: 32.0),
            child: Column(children: <Widget>[
              Form(
                  key: _key,
                  child: Column(children: <Widget>[
                    const Padding(padding: EdgeInsets.all(10.0)),
                    ListTile(
                      title: FutureBuilder<WebViewController>(
                          future: _controller.future,
                          builder: (BuildContext context,
                              AsyncSnapshot<WebViewController> controller) {
                            return TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Please enter your username";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: "Username",
                                  suffix: SizedBox(
                                    width: 16,
                                    height: 16,
                                    child: username != ""
                                        ? _isUserNameValidating
                                            ? CircularProgressIndicator(
                                                color: widget.textColor,
                                                strokeWidth: 2.0)
                                            : _isUserNameExists ? 
                                              const Icon(
                                                Icons.close,
                                                size: 18, 
                                                color: Colors.red
                                              ) : 
                                              const Icon(
                                                Icons.check,
                                                size: 18, 
                                                color: Colors.green
                                              )
                                        : null,
                                  ),
                                ),
                                onChanged: (value) async {
                                  username = value;
                                  setState(() {
                                    _isUserNameValidating = true;
                                  });
                                  bool isUserNameExists =
                                      (await isUserNameAvailable(value));
                                  setState(() {
                                    _isUserNameValidating = false;
                                    _isUserNameExists = isUserNameExists;
                                  });
                                });
                          }),
                    ),
                    Container(margin: const EdgeInsets.only(top: 16.0)),
                    ListTile(
                        title: TextFormField(
                      focusNode: _focus,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter your password";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: "Password",
                          suffixIcon: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 4, 0),
                            child: GestureDetector(
                              onTap: _togglePassObscured,
                              child: Icon(
                                _passObscured
                                    ? Icons.visibility_off_rounded
                                    : Icons.visibility_rounded,
                                size: 24,
                              ),
                            ),
                          )),
                      obscureText: _passObscured,
                      onChanged: (value) {
                        password = value;
                        setState(() {
                          passwordValidity = {
                            "minimum_length": password.length >= 8,
                            "number": password.contains(RegExp(r'[0-9]')),
                            "uppercase": password.contains(RegExp(r'[A-Z]')),
                            "lowercase": password.contains(RegExp(r'[a-z]')),
                            "special_character": password
                                .contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))
                          };
                        });
                      },
                    )),
                    Container(
                        margin: const EdgeInsets.only(bottom: 16.0),
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: _isValidationTextVisible
                            ? RichText(
                                text: TextSpan(children: <TextSpan>[
                                const TextSpan(text: "(Password must contain "),
                                TextSpan(
                                    text: "minimum 8 characters ",
                                    style: TextStyle(
                                        color: passwordValidity[
                                                    "minimum_length"] ==
                                                true
                                            ? Colors.green
                                            : Colors.red)),
                                const TextSpan(text: "and atleast one "),
                                TextSpan(
                                    text: "number, ",
                                    style: TextStyle(
                                        color:
                                            passwordValidity["number"] == true
                                                ? Colors.green
                                                : Colors.red)),
                                TextSpan(
                                    text: "uppercase, ",
                                    style: TextStyle(
                                        color: passwordValidity["uppercase"] ==
                                                true
                                            ? Colors.green
                                            : Colors.red)),
                                TextSpan(
                                    text: "lowercase, ",
                                    style: TextStyle(
                                        color: passwordValidity["lowercase"] ==
                                                true
                                            ? Colors.green
                                            : Colors.red)),
                                const TextSpan(text: "and "),
                                TextSpan(
                                    text: "special characters",
                                    style: TextStyle(
                                        color: passwordValidity[
                                                    "special_character"] ==
                                                true
                                            ? Colors.green
                                            : Colors.red)),
                                const TextSpan(text: ")")
                              ]))
                            : Container()),
                    ListTile(
                        title: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter your password";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: "Retype Password",
                          suffixIcon: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 4, 0),
                            child: GestureDetector(
                              onTap: _toggleReTypedPassObscured,
                              child: Icon(
                                _reTypedPassObscured
                                    ? Icons.visibility_off_rounded
                                    : Icons.visibility_rounded,
                                size: 24,
                              ),
                            ),
                          )),
                      obscureText: _reTypedPassObscured,
                      onChanged: (value1) {
                        reTypedPassword = value1;
                      },
                    )),
                    Container(
                        margin: const EdgeInsets.only(top: 8.0),
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        width: double.infinity,
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Checkbox(
                                checkColor: widget.primaryColor,
                                activeColor: widget.secondaryColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4.0)),
                                side: BorderSide(
                                    color: widget.secondaryColor, width: 2),
                                splashRadius: 30.0,
                                value: _isChecked,
                                onChanged: (bool? value) {
                                  setState(() {
                                    _isChecked = value!;
                                  });
                                },
                              ),
                              Flexible(
                                  child: RichText(
                                      text: TextSpan(
                                          children: const <TextSpan>[
                                    TextSpan(text: "I accept the "),
                                    TextSpan(
                                        text: "Terms of Services",
                                        style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            fontWeight: FontWeight.w700)),
                                    TextSpan(text: " and "),
                                    TextSpan(
                                        text: "Privacy Policy",
                                        style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            fontWeight: FontWeight.w700))
                                  ],
                                          style: TextStyle(
                                              color: widget.textColor,
                                              fontSize: 16.0))))
                            ]))
                  ])),
              _isChecked
                  ? Container(
                      margin: const EdgeInsets.only(top: 24.0),
                      padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                      child: setupAccount(),
                      height: 50.0,
                      width: double.infinity)
                  : Container(),
              Container(margin: const EdgeInsets.only(top: 48.0)),
              RichText(
                  text: TextSpan(
                      children: const <TextSpan>[
                    TextSpan(text: "Already have MOI Id? "),
                    TextSpan(
                        text: "Login here",
                        style: TextStyle(decoration: TextDecoration.underline))
                  ],
                      style: TextStyle(
                          color: widget.textColor,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w700)))
            ]))
      ];
    }

    List<Widget> getStep2() {
      String getMaskedPhrase(String secretRecoveryPhrase) {
        if (secretRecoveryPhrase.length > 2) {
          String maskedPhrase = secretRecoveryPhrase.substring(0, 2);
          for (int i = 0; i < secretRecoveryPhrase.length - 2; i++) {
            maskedPhrase += "*";
          }
          return maskedPhrase;
        }
        return "**";
      }

      void copySecretPhrase() {
        String secretRecoveryPhrase = secretPhrase
            .asMap()
            .entries
            .map(
                (element) => (element.key + 1).toString() + "." + element.value)
            .join("\n");
        Clipboard.setData(ClipboardData(text: secretRecoveryPhrase));
        setState(() {
          _isCopiedToClipboard = true;
          _isSecretPhraseCopied = true;
        });
        Future.delayed(const Duration(milliseconds: 1500), () {
          setState(() {
            _isCopiedToClipboard = false;
          });
        });
      }

      return [
        Container(
            padding: const EdgeInsets.only(left: 32.0, right: 32.0),
            child: GridView.builder(
                shrinkWrap: true,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    childAspectRatio: 2.0,
                    crossAxisSpacing: 4.0,
                    mainAxisSpacing: 4.0),
                itemCount: secretPhrase.length,
                itemBuilder: (context, index) =>
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                          child: Center(child: Text((index + 1).toString())),
                          decoration: BoxDecoration(
                              color: widget.secondaryColor,
                              borderRadius: BorderRadius.circular(50)),
                          height: 28.0,
                          margin: const EdgeInsets.only(right: 8.0),
                          width: 28.0),
                      Text(getMaskedPhrase(secretPhrase[index]))
                    ]))),
        Container(
            margin: const EdgeInsets.only(top: 16.0),
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: const Text(
                "For your protection Global Digital Footprint is masked. Keep the Global Digital Footprint stored in a safe place. It is the only way to restore your identity if you lose or forget your password.",
                textAlign: TextAlign.center)),
        InkWell(
          child: Container(
              alignment: Alignment.center,
              child: const Icon(Icons.copy, size: 18.0),
              decoration: BoxDecoration(
                  color: widget.secondaryColor,
                  borderRadius: BorderRadius.circular(50)),
              height: 48.0,
              margin: const EdgeInsets.only(top: 16.0),
              width: 48.0),
          onTap: copySecretPhrase,
        ),
        Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: Text(_isCopiedToClipboard ? "Copied" : "Copy")),
        _isSecretPhraseCopied ? getNextButton() : Container()
      ];
    }

    List<Widget> getOptions() {
      // secretPhrase
      return challengeOptions.asMap().entries.map((entry) {
        return InkWell(
            child: Container(
                alignment: Alignment.center,
                child: Text(entry.value,
                    style: TextStyle(
                        color: entry.key == _selectedOption
                            ? widget.primaryColor
                            : widget.textColor)),
                decoration: BoxDecoration(
                    border: Border.all(color: widget.textColor, width: 1.5),
                    borderRadius: BorderRadius.circular(4.0),
                    color: entry.key == _selectedOption
                        ? widget.textColor
                        : widget.primaryColor),
                height: 48.0,
                margin:
                    const EdgeInsets.only(top: 21.0, left: 16.0, right: 16.0),
                width: double.infinity),
            onTap: () => {
                  setState(() {
                    _selectedOption = entry.key;
                  })
                });
      }).toList();
    }

    List<Widget> getStep3() {
      return [
        Container(
            margin: const EdgeInsets.only(top: 32.0),
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: Text(
                "What is your " +
                    correctOrder[0].toString() +
                    " and " +
                    correctOrder[1].toString() +
                    " words of your Global digital footprint?",
                textAlign: TextAlign.center)),
        ...getOptions(),
        _selectedOption != -1 ? getNextButton() : Container()
      ];
    }

    switch (_currentStep) {
      case 1:
        return getStep2();
      case 2:
        return getStep3();
      default:
        return getStep1();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: widget.primaryColor,
        body: SingleChildScrollView(
            child: Column(children: [
          const Padding(padding: EdgeInsets.all(10.0)),
          Container(
              margin: const EdgeInsets.only(top: 100.0, bottom: 32.0),
              child: Text(_getTitle(),
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontSize: 20.0, fontWeight: FontWeight.bold)),
              width: double.infinity),
          Container(
              padding: const EdgeInsets.only(left: 32.0, right: 32.0),
              child: Row(children: <Widget>[
                Container(
                    child: const Center(child: Text("1")),
                    decoration: BoxDecoration(
                        color: widget.secondaryColor,
                        borderRadius: BorderRadius.circular(50)),
                    height: _currentStep == 0 ? 48.0 : 36.0,
                    width: _currentStep == 0 ? 48.0 : 36.0),
                Expanded(
                    child: Divider(
                        color: _currentStep > 0
                            ? widget.secondaryColor
                            : widget.textColor,
                        thickness: 3)),
                Container(
                    child: const Center(child: Text("2")),
                    decoration: BoxDecoration(
                        color: widget.secondaryColor,
                        borderRadius: BorderRadius.circular(50)),
                    height: _currentStep == 1 ? 48.0 : 36.0,
                    width: _currentStep == 1 ? 48.0 : 36.0),
                Expanded(
                    child: Divider(
                        color: _currentStep > 1
                            ? widget.secondaryColor
                            : widget.textColor,
                        thickness: 3)),
                Container(
                    child: const Center(child: Text("3")),
                    decoration: BoxDecoration(
                        color: widget.secondaryColor,
                        borderRadius: BorderRadius.circular(50)),
                    height: _currentStep == 2 ? 48.0 : 36.0,
                    width: _currentStep == 2 ? 48.0 : 36.0),
              ]),
              width: double.infinity),
          ..._getStep(),
          Container(
              margin: const EdgeInsets.only(top: 24.0, bottom: 24.0),
              width: double.infinity,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text("POWERED BY ",
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w700)),
                    Image.network('https://moi-id.life/assets/logo.png',
                        height: 64.0, width: 64.0)
                  ])),
          Visibility(
            visible: false,
            maintainState: true,
            child: SizedBox(
                height: 1,
                child: WebView(
                    initialUrl: 'https://zkp.netlify.app',
                    javascriptMode: JavascriptMode.unrestricted,
                    onWebViewCreated: (WebViewController webViewController) {
                      _controller.complete(webViewController);
                    },
                    javascriptChannels: <JavascriptChannel>{
                      _toasterJavascriptChannel(context),
                    },
                    onProgress: (int per) {
                      print("Loaded: $per %");
                    },
                    onPageFinished: (String url) {
                      print("start now");
                      // Future.delayed(const Duration(milliseconds: 1000), () {
                      //   setState(() {
                      //     isWebViewLoaded = true;
                      //   });
                      // });
                    },
                    gestureNavigationEnabled: true,
                    backgroundColor: const Color(0x00000000))),
          ),
        ])));
  }
}
